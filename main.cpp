//
// Created by Emily on 11/12/2018.
//

//used https://www.geeksforgeeks.org/strassens-matrix-multiplication/

//https://www.cs.mcgill.ca/~pnguyen/251F09/matrix-mult.pdf

//divide matrix into 4 sub parts
#include <iostream>
#include <vector>
#include <iomanip>

using namespace std;



//int[][] divideMatrix(unsigned int size,int m[size][size]) {
vector<vector<vector<int>>> divideMatrix(vector<vector<int>> m, unsigned int size){
    int results[4][size/2][size/2];
    vector<vector<vector<int>>> vec(4, std::vector<std::vector<int>>(size/2, std::vector<int>(size/2)));

    for(unsigned int i = 0; i < size/2; i++) {
        for(unsigned int j = 0; j < size/2; j++) {
            //results[0][i][j] = m[i][j];
            vec.at(0).at(i).at(j) = m[i][j];

            //results[1][i][j] = m[i][size/2+j];
            vec.at(1).at(i).at(j) = m[i][size/2+j];

            //results[2][i][j] = m[size/2+i][j];
            vec.at(2).at(i).at(j) = m[size/2+i][j];

            //results[3][i][j] = m[size/2+i][size/2+j];
            vec.at(3).at(i).at(j) = m[size/2+i][size/2+j];
        }
    }
    return vec;
}


//combine 4 sub matrices into one
vector<vector<int>> combineMatrix(vector<vector<vector<int>>> results, unsigned int size) {
    int m[size][size];

    vector<vector<int>> vec(size, std::vector<int>(size));

    for (unsigned int i = 0; i < size/2; i++) {
        for(unsigned int j = 0; j < size/2; j++) {
            m[i][j] = results[0][i][j];
            vec.at(i).at(j) = results[0][i][j];

            m[i][size / 2 + j] = results[1][i][j];
            vec.at(i).at(size/2 + j) = results[1][i][j];

            m[size / 2 + i][j] = results[2][i][j];
            vec.at(size/2 + i).at(j) = results[2][i][j];

            m[size / 2 + i][size / 2 + j] = results[3][i][j];
            vec.at(size/2 + i).at(size/2 + j) = results[3][i][j];
        }
    }

    return vec;
}

vector<vector<int>> addMatrix(vector<vector<int>> m1, vector<vector<int>> m2, unsigned int size){
    vector<vector<int>> results(size,vector<int>(size));

    for(int i = 0; i < size; i++){
        for(int j = 0; j < size; j++){
            results[i][j] = m1[i][j] + m2[i][j];
        }
    }

    return results;
}

//calculates based on dividing and finding subresult from dividing
vector<vector<int>> multiplyMatrix(vector<vector<int>> m1, vector<vector<int>> m2, unsigned int size) {
    if(size == 1) {
        vector<vector<int>> result(1,std::vector<int>(1,m1[0][0] * m2[0][0]));
        return result;
    }
    //int resultsA[4][size/2][size/2] = divideMatrix(m1, size);
    vector<vector<vector<int>>> resultsA(4, std::vector<std::vector<int>>(size/2, std::vector<int>(size/2)));
    resultsA = divideMatrix(m1,size);

    //int resultsB[4][size/2][size/2] = divideMatrix(m2, size);
    vector<vector<vector<int>>> resultsB(4, std::vector<std::vector<int>>(size/2, std::vector<int>(size/2)));
    resultsB = divideMatrix(m2,size);

    //int resultC[4][size/2][size/2];
    vector<vector<vector<int>>> resultC(4, std::vector<std::vector<int>>(size/2, std::vector<int>(size/2)));

    //replace + and add remaining sub multiplications
    resultC[0] = addMatrix(multiplyMatrix(resultsA[0], resultsB[0],size/2), multiplyMatrix(resultsA[1], resultsB[2], size/2),size/2);
    resultC[1] = addMatrix(multiplyMatrix(resultsA[0], resultsB[1],size/2), multiplyMatrix(resultsA[1], resultsB[3], size/2),size/2);
    resultC[2] = addMatrix(multiplyMatrix(resultsA[2], resultsB[0],size/2), multiplyMatrix(resultsA[3], resultsB[2], size/2),size/2);
    resultC[3] = addMatrix(multiplyMatrix(resultsA[2], resultsB[1],size/2), multiplyMatrix(resultsA[3], resultsB[3], size/2), size/2);

    return combineMatrix(resultC, size);
}




int main() {

   // int m1[4][4] = {{2,1,0,0}, {1,2,1,1}, {0,0,2,1}, {2,2,1,0}};
    vector<vector<int>> m = {{2,1,0,0,7,7,9,8}, {1,2,1,10,7,8,9,0}, {0,0,2,1,13,4,6,8}, {2,2,1,0,4,3,6,8}, {3,5,6,8,0,2,4,6},
            {1,5,6,7,2,4,3,1}, {2,6,5,2,7,8,4,5}, {1,3,5,6,8,3,4,7}};
    unsigned int SIZE = 8;

    cout << "Inital Matrix: " << endl;
    for(int i = 0; i < SIZE; i++){
        for(int k = 0; k < SIZE; k++){
            cout << setw(3) << left << m[i][k];
        }
        cout << endl;
    }
    cout << endl;

    vector<vector<vector<int>>> result;

    result = divideMatrix(m,SIZE);

    int  n = SIZE/2;

    cout << "Divided Matrix: " << endl;
    for(int i = 0; i < n; i++) {
        for(int j = 0; j < n; j++) {
            for(int k = 0; k < n; k++) {
                cout << setw(3) << left << result[i][j][k];
            }
            cout << endl;
        }
        cout << endl;
    }


    cout << endl;
    //combine matrix back together
    cout << "Combine Matrix back together: " << endl;
    vector<vector<int>> combine = combineMatrix(result,SIZE);
    for(int t = 0; t < SIZE; t++){
        for(int r = 0; r < SIZE; r++){
            cout << setw(3) << left << combine[t][r];
        }
        cout << endl;
    }

    cout << endl;
    //multiply matrix
    vector<vector<int>> m2 = {{2,1,0,0,7,7,9,8}, {1,2,1,10,7,8,9,0}, {0,0,2,1,13,4,6,8}, {2,2,1,0,4,3,6,8}, {3,5,6,8,0,2,4,6},
                              {1,5,6,7,2,4,3,1}, {2,6,5,2,7,8,4,5}, {1,3,5,6,8,3,4,7}};

    vector<vector<int>> multi;

    multi = multiplyMatrix(m,m2,SIZE);

    cout << "Multiply Matrices: " << endl;
    for(int t = 0; t < SIZE; t++){
        for(int r = 0; r < SIZE; r++){
            cout << setw(5) << left << multi[t][r];
        }
        cout << endl;
    }



    return 0;

}

